const fieldTypes = {
    checkbox: 'Checkbox',
    date: 'Date',
    datetime: 'Date with Time',
    dropdown: 'Dropdown',
    file_upload: 'File Upload',
    radio: 'Radio',
    signature: 'Signature',
    text: 'Text',
    time: 'Time',
    payment: 'Payment',
    appointment: 'Appointment'
};

export default fieldTypes